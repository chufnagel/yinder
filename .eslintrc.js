module.exports = {
  "parser": "babel-eslint",  
  "extends": "airbnb-base",
  "plugins": [
    "react",
    "jsx-a11y",
    "import",
  ],
  "parserOptions": {
    "ecmaVersion": 9,
    "sourceType": "module",
    "ecmaFeatures": {
      "jsx": true
    }
  },
  "env": {
    "es6": true,
    "node": true
  },
    rules: {
      'no-console': 'off',
      'no-unused-vars': 'off',
      'space-before-function-paren': 'off',
      'func-names': 'off',
      'no-shadow': 'off',
      'camelcase': 'off',
      'object-shorthand': 'off',
      'linebreak-style': 'off',
      'prefer-destructuring': 'off',
      'arrow-parens': 'off',
    }
  };