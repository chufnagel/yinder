const path = require('path');
// const autoprefixer = require('autoprefixer');
// const HtmlWebpackPlugin = require('html-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');

const SRC_DIR = path.join(__dirname, '/client/src');
const DIST_DIR = path.join(__dirname, '/client/dist');

module.exports = {
  devtool: 'cheap-source-map',
  entry: `${SRC_DIR}/index.jsx`,
  mode: 'production',
  output: {
    path: DIST_DIR,
    filename: 'bundle.js',
  },
  context: __dirname,
  resolve: {
    extensions: ['.js', '.jsx', '.json', '*'],
  },
  // optimization: {
  //   splitChunks: {
  //     chunks: 'all',
  //   },
  //   minimize: true,
  // },
  module: {
    rules: [
      {
        test: /\.jsx?/,
        include: SRC_DIR,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.s?css$/,
        use: [
          'css-loader',
          'sass-loader',
          'style-loader',
        ],
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url-loader?limit=10000&mimetype=application/font-woff',
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader',
      },
    ],
  },
  plugins: [
    // new HtmlWebpackPlugin({
    //   title: 'Yinder',
    //   favicon: `${SRC_DIR}/favicon.ico`,
    //   template: `${SRC_DIR}/index.html`,
    //   filename: 'index.html',
    //   inject: 'body',
    // }),
    new ManifestPlugin({
      fileName: 'asset-manifest.json',
      basePath: `${DIST_DIR}`,
    }),
  ],
};
